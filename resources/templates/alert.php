<?php
/**
 * @desc    Alert templates. Includes error, warning and succes alert banners.
 */

function alert($alert) {

if (@$alert['type'] == "warning") { ?>
<!-- Warning -->
<div class="alert alert-warning" role="alert">			
	<i class="fad fa-exclamation-triangle mr-2"></i><strong>Warning!</strong> <?=$alert['message']?>
	<?php if ($alert['close'] == true) { ?>
	<!-- Close button -->
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<i class="far fa-times"></i>
	</button>
	<?php } ?>
</div>

<?php } elseif (@$alert['type'] == "error") { ?>
<!-- Error -->
<div class="alert alert-danger" role="alert">			
	<i class="fad fa-exclamation-triangle mr-2"></i><strong>Error!</strong> <?=$alert['message']?>
	<?php if ($alert['close'] == true) { ?>
	<!-- Close button -->
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<i class="far fa-times"></i>
	</button>
	<?php } ?>
</div>

<?php } elseif (@$alert['type'] == "success") { ?>
<!-- Success -->
<div class="alert alert-success" role="alert">			
	<i class="fad fa-check-double mr-2"></i><strong>Success!</strong> <?=$alert['message']?>
	<?php if ($alert['close'] == true) { ?>
	<!-- Close button -->
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<i class="far fa-times"></i>
	</button>
	<?php } ?>
</div>

<?php }} ?>