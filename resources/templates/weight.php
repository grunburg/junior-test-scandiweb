<div class="col-lg-4 mb-3 product" id="<?=$id;?>">
	<div class="card">
		<div class="card-header">
			<div class="clearfix">
				<span class="text-muted font-weight-bold float-left">#<?=$sku;?></span>
				<div class="custom-control custom-checkbox float-right">
					<input type="checkbox" class="custom-control-input product-checkbox" id="checkid<?=$id;?>" name="check[]" value="<?=$id;?>">
					<label class="custom-control-label" for="checkid<?=$id;?>"></label>
				</div>
			</div>
		</div>
		<div class="card-body">
			<h5 class="card-title"><?=$name;?><span class="badge badge-primary text-light float-right">$ <?=$price;?></span></h5>
			<div class="clearfix mb-2">
				<h6 class="card-subtitle text-muted float-left">Type</h6>
				<h6 class="card-subtitle text-muted float-right">Weight</h6>
			</div>
			<ul class="list-group list-group-horizontal-md text-center">
				<li class="list-group-item font-weight-bold text-muted flex-fill bg-light">Weight</li>
				<li class="list-group-item font-weight-bold text-muted flex-fill"><?=$value;?> Kg</li>
			</ul>
		</div>
	</div>
</div>