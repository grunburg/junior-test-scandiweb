<?php
/**
 * @desc    Main configuration file.
 *          Please change folder path also in db/connect.php in variable $PATH,
 *			didn't know how to fix 'include' interference.
 */

return [
	//database connection info -> classes/connect.class.php
    "database" => [
        "host" => "hostname",
        "user" => "username",
        "pass" => "password",
        "name" => "database"
    ],
    //path to this project from main 'htdocs', 'wwww' folder, both "/" must be included  -> inc/template.inc.php
    "path" => "/folder/",
];

?>