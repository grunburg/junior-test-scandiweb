<?php
/**
 * @desc    Handles all connection to the database.
 */

class Connect
{
	private $host;
	private $user;
	private $pass;
	private $name;

	private $db;

	public function connect()
	{	
		//path to this project from main 'htdocs', 'wwww' folder, both "/" must be included
		$PATH = "/folder/";
		$this->db = include ($_SERVER["DOCUMENT_ROOT"] . $PATH . 'config.php');

		//database connection info
		$this->host = $this->db['database']['host'];
		$this->user = $this->db['database']['user'];
		$this->pass = $this->db['database']['pass'];
		$this->name = $this->db['database']['name'];

		//make connection with the database
		$mysql = new mysqli($this->host, $this->user, $this->pass, $this->name);

		//checks and creates connection with database
		return $mysql;
	}
}
?>