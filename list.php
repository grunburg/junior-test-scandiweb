<?php
//including header
include ('inc/header.inc.php');

//declaring the class
$view = new View();
?>

<!-- Main -->
<main>
	<div class="container">
		<ul class="nav my-3">
			<li class="nav-item mr-auto">
				<span class="nav-link pl-0 text-muted"><?=$PAGE;?></span>
			</li>
			<li class="nav-item ml-auto">
				<button type="button" class="btn btn-primary mr-1" id="delCheck">Delete Checked</button>
				<button type="button" class="btn btn-danger" id="delAll"><i class="fad fa-trash-alt"></i> Delete All</button>				
			</li>
		</ul>
		<div class="content">
			<div class="row">
				<?php
				//displaying products
				$view->viewProducts();
				?>
			</div>
		</div>
	</div>
</main>

<?php
//including footer
include ('inc/footer.inc.php');
?>