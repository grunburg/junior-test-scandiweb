<?php
/**
 * @desc    A delete button requesting to delete checked or all items.
 */

//includes delete.class.php classes & functions
include ('../../classes/delete.class.php');

//declaring the class
$delete = new Delete;

if ($_POST['type'] == "delCheck") {
    $delete->delCheck();
} else if ($_POST['type'] == "delAll") {
    $delete->delAll();
}
?>