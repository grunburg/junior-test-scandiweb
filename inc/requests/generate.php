<?php
/**
 * @desc    A genereate button requesting to generate a unique SKU code.
 */

//includes generate.class.php classes & functions
include ('../../classes/generate.class.php');

//declaring the class
$sku = new Generate();
//outputs generated key
echo $sku->generateSku();
?>