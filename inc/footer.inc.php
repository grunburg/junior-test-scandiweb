<!-- Footer -->
<footer class="text-muted mb-3">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
			    <div class="card">
			      <div class="card-body">
			        	<h5 class="card-title"><i class="fad fa-brackets-curly text-primary"></i> Junior Test</h5>
			        	<p class="card-text">By doing Junior Test I have improved my knowledge both in <code>&lt;javascript&gt;</code> and <code>&lt;?php&gt;</code>. More about my Junior Test here <a href="#"><mark class="bg-light"><code class="text-primary">README.md</code></mark></a></p>
			      </div>
			    </div>
			</div>
			<div class="col-lg-6">
			    <div class="card costum">
			      <div class="card-body">
			        	<h5 class="card-title"><i class="far fa-universal-access"></i> Costum Card</h5>
			        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				  		<button type="button" class="btn btn-primary">Button</button>
					</div>
			    </div>
			</div>
		</div>
	</div>
</footer>

<!-- Boostrap -->
<script src="assets/js/bootstrap/bootstrap.min.js"></script>

<!-- Fontawesome Icons -->
<script src="assets/js/fontawesome/fontawesome.js"></script>

<!-- Custom Javascript -->
<script src="assets/js/switch.js"></script>
<script src="assets/js/delete.js"></script>

</body>
</html>