<?php
/**
 * @desc    A small Templating "engine" as of frameworks were not allowed
 */

//includes config file
$PATH = include ('config.php');

    switch ($_SERVER["PHP_SELF"]) {
        //if "Product List" page is active
        case $PATH['path'] . "list.php":
            $PAGE = "Product List"; 
            break;
        //if "New Product" page is active
        case $PATH['path'] . "new.php":
            $PAGE = "New Product";
            break;
    }
?>