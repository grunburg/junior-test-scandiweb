<?php
//requires both autoload & templates
include ('inc/autoload.inc.php');
include ('inc/template.inc.php');
include ('resources/templates/alert.php');
?>

<!-- Header -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="assets/css/styles.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<link rel="shortcut icon" href="assets/img/favicon.png" />
	<title>Junior Test - <?=$PAGE;?></title>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
	<div class="container">
		<a class="navbar-brand" href="#">Junior Test</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navigation">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item <?php if($PAGE == 'Product List'){print'active';} ?>">
					<a class="nav-link" href="list.php">
						Product List
					</a>
				</li>
				<li class="nav-item <?php if($PAGE == 'New Product'){print'active';} ?>">
					<a class="nav-link" href="new.php">
						New Product
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>