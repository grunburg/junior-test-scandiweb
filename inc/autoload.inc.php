<?php
/**
 * @desc    Autoloads .class.php files.
 */

function autoload($class)
{
    $folder = "classes/";
    $extension = ".class.php";
    $path = $folder . $class . $extension;

    //includes full path to class files
    include $path;
}

//autoload magic...
spl_autoload_register('autoload');
?>