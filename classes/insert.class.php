<?php
/**
 * @desc    Fetches checked data from check.class.php and inserts it into the database.
 *          If database table doesn't exsist, function create() automatically creates it.
 */

//includes connection
include ('../../db/connect.php');

class Insert extends Connect
{
    //fetches passed data and inserts into the database
    protected function type($type)
    {
        //declare statement variable as $stmt
        $con = $this->connect();
        $stmt = $con->stmt_init();

        switch ($type) {
            //if passed type is 'size'
            case "size":
                //sql statement to run in database
                $sql = "INSERT INTO products (sku, name, price, type, value) VALUES (?, ?, ?, ?, ?)";

                //check for errors
                if (!$stmt->prepare($sql)) {
                    //return to new.php page with error message
                    header("Location: ../../new.php?error=mysql");
                    exit();
                } else {
                    //bind variables to a prepared statement & execute
                    $stmt->bind_param("ssdsd", $this->sku, $this->name, $this->price, $this->switch, $this->size);
                    $stmt->execute();

                    //return to new.php page with success message
                    header("Location: ../../new.php?success=new");
                    exit();
                }
            break;
            //if passed type is 'weight'
            case "weight":
                //sql statement to run in database
                $sql = "INSERT INTO products (sku, name, price, type, value) VALUES (?, ?, ?, ?, ?)";

                //check for errors
                if (!$stmt->prepare($sql)) {
                    //return to new.php page with error message
                    header("Location: ../../new.php?error=mysql");
                    exit();
                } else {
                    //bind variables to a prepared statement & execute
                    $stmt->bind_param("ssdsd", $this->sku, $this->name, $this->price, $this->switch, $this->weight);
                    $stmt->execute();

                    //return to new.php page with success message
                    header("Location: ../../new.php?success=new");
                    exit();
                }
            break;
            //if passed type is 'dimensions'
            case "dimensions":
                //sql statement to run in database
                $sql = "INSERT INTO products (sku, name, price, type, value) VALUES (?, ?, ?, ?, ?)";

                //check for errors
                if (!$stmt->prepare($sql)) {
                    //return to new.php page with error message
                    header("Location: ../../new.php?error=mysql");
                    exit();
                } else {
                    //stores height, width, length values in a array
                    $hwl = array($this->height, $this->width, $this->length);
                    //array values are joined togheter as string. e.g. 1-8-6
                    $param = implode("-", $hwl);

                    //bind variables to a prepared statement & execute
                    $stmt->bind_param("ssdss", $this->sku, $this->name, $this->price, $this->switch, $param);
                    $stmt->execute();

                    //return to new.php page with success message
                    header("Location: ../../new.php?success=new");
                    exit();
                }
            break;
        }
    }

    //creates new database table if it does not exsist
    protected function create()
    {
        //declare connection
        $con = $this->connect();

        //sql statement to run in database
        $sql = "SELECT 1 FROM products LIMIT 1";
        $result = $con->query($sql);

        //if result equals false -> create new database table
        if ($result == false) {
            //get file contents from create.sql
            $file = file_get_contents("../../db/create.sql");
            $con->query($file);
        }
    }
}
?>