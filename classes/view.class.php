<?php
/**
 * @desc    Displays products from database.
 */

class View extends Select
{
	//view products from database
	public function viewProducts()
	{
		$select = $this->selectProducts();
		if (is_array($select)) {
			foreach ($select as $data) {
				//declaring variables
				$id = $data['id'];
				$sku = $data['sku'];
				$name = $data['name'];
				$price = $data['price'];
				$type = $data['type'];
				$value = $data['value'];

				//if data type is size
				if ($type === "size") {
					include "resources/templates/size.php";
				}
				//if data type is weight
				elseif ($type === "weight") {
					include "resources/templates/weight.php";
				}
				//if data type is dimensions
				elseif ($type === "dimensions") {
					//separates value to three different variables: $height, $width, $length by removing '-'
					list($height, $width, $length) = explode("-", $value);
					include "resources/templates/dimensions.php";
				}
			}
		}
	}
}
?>