<?php
/**
 * @desc    Selects and retruns data from database.
 */

//includes connection
require ('db/connect.php');

class Select extends Connect
{
	//selects products from database
	protected function selectProducts()
	{
		//sql statement to run in database
		$sql = "SELECT * FROM products ORDER BY id DESC";

		//performs a query on the database
		$result = $this->connect()->query($sql);
		$num = @$result->num_rows;
		//retrieves records
		if ($num > 0) {
			while ($row = $result->fetch_assoc()) {
				$data[] = $row;
			}
			return $data;
		} else {
			//alert to display if table is not found
			$alert = [
				"message" => "Data could not be found. Add a <a href='new.php' class='alert-link font-weight-bold'>New Product</a>.",
				"close" => false,
				"type" => "warning"
			];
			print "<div class='col-12'>";
			//calling alert function
			print alert($alert);
			print "</div>";
		}
	}
}
?>