<?php
/**
 * @desc    Deletes checked or all items from database.
 */

//includes connection
include ('../../db/connect.php');

class Delete extends Connect
{
    public $prodid;

    //deletes selected items
    public function delCheck()
    {
        //declare statement variable as $stmt
        $con = $this->connect();
        $stmt = $con->stmt_init();

        if (isset($_POST['id'])) {
            foreach ($_POST['id'] as $this->prodid) {
                //sql statement to run in database
                $sql = "DELETE from products WHERE id = ?";
                $stmt->prepare($sql);

                //bind variables to a prepared statement & execute
                $stmt->bind_param("i", $this->prodid);
                $stmt->execute();
            }
        }
    }

    //deletes all items
    public function delAll()
    {
        //declare connection
        $con = $this->connect();

        //sql statement to run in database
        $sql = "DELETE FROM products";
        $con->query($sql);
    }       
}
?>