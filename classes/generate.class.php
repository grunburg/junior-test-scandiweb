<?php
/**
 * @desc    Generates a unique SKU string.
 *          If generated string already exists in database, generate another string until it doesnt match.
 */

//includes connection
include_once ('../../db/connect.php');

class Generate extends Connect
{
    //checks if generated string already exists in database
    public function check($str)
    {
        $con = $this->connect();

        //sql statement to run in database
        $sql = "SELECT sku FROM products";
        $result = $con->query($sql);

        while ($row = $result->fetch_assoc()) {
            if ($row['sku'] == $str) {
                $exists = true;
                break;
            } else {
                $exists = false;   
            }
        }
        //retruns boolean
        return @$exists;
    }

    //generates strings
    public function generateSku()
    {
        //every character that generates unique string
        $char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        //character shuffle
        $str = substr(str_shuffle($char), 0, 6);

        //declare connection
        $con = $this->connect();

        //sql statement to run in database
        $sql = "SELECT 1 FROM products LIMIT 1";
        $result = $con->query($sql);

        //if result equals false -> generate new unique string
        if ($result == false) {
            return $str;
        //else generate string that isn't in database
        } else {
            $check = $this->check($str);

            while ($check == true) {
                $str = substr(str_shuffle($char), 0, 6);
                $check = $this->check($str);
            }

            //returns generated string
            return $str;   
        }
    }
}
?>