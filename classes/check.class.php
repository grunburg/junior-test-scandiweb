<?php
/**
 * @desc    Checks & passes data to insert.class.php.
 */

//includes insert.class.php functions where data is latter passed
include ('insert.class.php');

class Check extends Insert
{
    public $sku;
    public $name;
    public $size;
    public $price;
    public $switch;
    public $weight;
    public $height;
    public $width;
    public $length;

    public function __construct()
    {
        //declaring variables
        $this->sku = $_POST['sku'];
        $this->name = $_POST['name'];
        $this->size = $_POST['size'];
        $this->price = $_POST['price'];
        $this->switch = $_POST['switch'];
        $this->weight = $_POST['weight'];
        $this->height = $_POST['height'];
        $this->width = $_POST['width'];
        $this->length = $_POST['length'];

        //if 'submit' button is clicked
        if (isset($_POST['submit'])) {

            $this->create();

            if (empty($this->sku) || empty($this->name) || empty($this->price) || empty($this->switch)) {
                //return to new.php page with error message
                header("Location: ../../new.php?error=empty");
                exit();
            } elseif (!preg_match("/^[A-Z0-9]+$/", $this->sku) || !preg_match("/^\d+(\.\d{2})?$/", $this->price)) {
                //return to new.php page with error message
                header("Location: ../../new.php?error=preg");
                exit(); 
            } else {
                //using switch statement...
                switch ($this->switch) {
                    //if type 'size' is selected
                    case "size":
                        if (empty($this->size)) {
                            //return to new.php page with error message
                            header("Location: ../../new.php?error=type");
                            exit();
                        } else {
                            //calling 'type' function from insert.php
                            $this->type("size");
                        }
                    break;
                    //if type 'weight' is selected
                    case "weight":
                        if (empty($this->weight)) {
                            //return to new.php page with error message
                            header("Location: ../../new.php?error=type");
                            exit();
                        } else {
                            //calling 'type' function from insert.php
                            $this->type("weight");
                        }
                    break;
                    //if type 'dimensions' is selected
                    case "dimensions":
                        if (empty($this->height) || empty($this->width) || empty($this->length)) {
                            //return to new.php page with error message
                            header("Location: ../../new.php?error=type");
                            exit();
                        } else {
                            //calling 'type' function from insert.php
                            $this->type("dimensions");
                        }
                    break;
                    //if something has gone wrong
                    default:
                        header("Location: ../../new.php?error=default");
                        exit();
                    break;
                }
            }
        }
    }
}
?>