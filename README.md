### Junior Test
By Ričards Grīnbergs

------------------

### Usage

Open `config.php` and enter MySQL connection info and project **path**.

⚠️ I couldn't figure out how to fix 'include' interference, so you will need to change folder `$path` in `/db/connect.php` too. Sorry!

*  Database table is created automatically after pressing **Add Product**.
*  SKU generates automatically.

------------------

Tools & Technologies I used:

*  SCSS
*  PHP
*  jQuery
*  MySQL
*  HTML
*  Node.js