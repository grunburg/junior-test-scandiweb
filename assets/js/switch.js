/**
 * @desc    Displays input fields from drop-down selection.
 */

$(document).ready(function () {
    $("#switch").change(function () {
        //if selected option is size
        if ($(this).val() === "size") {
            $(".switch-body").html(`
            <label for="size">Size</label>
            <div class="input-group">
                <input type="text" class="form-control no-comma" id="size" placeholder="Product Size" aria-describedby="sizeHelp" name="size">
                <div class="input-group-append">
                    <div class="input-group-text"><code class="text-muted">MB</code></div>
                </div>            
            </div>
            <small id="sizeHelp" class="form-text text-muted"><i class="fad fa-exclamation-square fa-lg text-primary"></i> Please provide your product Size in <u>Megabytes</u>.</small>
            `);
        }
        //if selected option is weight
        else if ($(this).val() === "weight") {
            $(".switch-body").html(`
            <label for="weight">Weight</label>
            <div class="input-group">
                <input type="text" class="form-control no-comma" id="weight" placeholder="Product Weight" aria-describedby="weightHelp" name="weight">
                <div class="input-group-append">
                    <div class="input-group-text text-muted">Kg</div>
                </div>            
            </div>
            <small id="weightHelp" class="form-text text-muted"><i class="fad fa-exclamation-square fa-lg text-primary"></i> Please provide your product Weight in <u>Kilograms</u>.</small>
            `);
        }
        //if selected option is weight
        else if ($(this).val() === "dimensions") {
            $(".switch-body").html(`
            <label class="text-muted">Dimensions</label>
            <div class="form-row">
                <div class="form-group col-md-4 mb-md-0">
                    <div class="input-group">
                        <input type="text" class="form-control no-comma" id="height" placeholder="Height" name="height">
                        <div class="input-group-append">
                            <div class="input-group-text text-muted">M</div>
                        </div>                      
                    </div>
                </div>
                <div class="form-group col-md-4 mb-md-0">
                    <div class="input-group">
                        <input type="text" class="form-control no-comma" id="width" placeholder="Width" name="width">
                        <div class="input-group-append">
                            <div class="input-group-text text-muted">M</div>
                        </div>                      
                    </div>
                </div>
                <div class="form-group col-md-4 mb-md-0">
                    <div class="input-group">
                        <input type="text" class="form-control no-comma" id="length" placeholder="Length" name="length">
                        <div class="input-group-append">
                            <div class="input-group-text text-muted">M</div>
                        </div>                      
                    </div>
                </div>
            </div>
            <small id="weightHelp" class="form-text text-muted"><i class="fad fa-exclamation-square fa-lg text-primary"></i> Please provide your product Height, Width & Length in <u>Meters</u>.</small>
            `);
        }
        //if selected option is none
        else if ($(this).val() === "") {
            $(".switch-body").html("");
        }
    });
});