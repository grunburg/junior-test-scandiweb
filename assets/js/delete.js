/**
 * @desc    Deletes selected or all items.
 */
 
$(document).ready(function(){

    //deletes checked items
    $("#delCheck").click(function() {
        //empty array in variable
        var checkid = [];

        $(".product-checkbox:checked").each(function(i){
            checkid[i] = $(this).val();

            //send ajax request
            $.ajax({
                url: "inc/requests/delete.php",
                method: "POST",
                data: {
                    id: checkid,
                    type: "delCheck"
                },
                success: function()
                {
                    for(var i=0; i<checkid.length; i++)
                    {
                        $(".product#" + checkid[i]).fadeOut();
                    }
                }
            });
        });
    });

    //deletes all items
    $("#delAll").click(function() {

        //send ajax request
        $.ajax({
            url: "inc/requests/delete.php",
            method: "POST",
            data:
            {
                type: "delAll"
            },
            success: function()
            {
                $(".product").fadeOut();
            }
        });
    });
});